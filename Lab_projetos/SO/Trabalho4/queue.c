#include "queue.h"


#ifndef NULL
#define NULL ((void *) 0)
#endif

//------------------------------------------------------------------------------
// Insere um elemento no final da fila.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - o elemento deve existir
// - o elemento nao deve estar em outra fila

void queue_append (queue_t **queue, queue_t *elem)
{
    if(queue)
    {
        if(elem)
        {
            if((elem->next == NULL) && (elem->prev == NULL))
            {
                if(*queue)
                {
                    queue_t *ultimo = (*queue)->prev;

                    ultimo->next = elem;
                    elem->prev = ultimo;

                    elem->next = *queue;
                    (*queue)->prev = elem;

                }
                else
                {
                    elem->next = elem;
                    elem->prev = elem;
                    *queue = elem;
                }
            }
            else
                perror ("Elemento em outra fila");
        }
        else
            perror ("Erro no elemento");
    }
    else
        perror ("Erro na fila");
}

//------------------------------------------------------------------------------
// Remove o elemento indicado da fila, sem o destruir.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - a fila nao deve estar vazia
// - o elemento deve existir
// - o elemento deve pertencer a fila indicada
// Retorno: apontador para o elemento removido, ou NULL se erro

queue_t *queue_remove (queue_t **queue, queue_t *elem)
{
    if(queue)
    {
        if(*queue)
        {
            if(elem)
            {
                int element = 0;
                queue_t *aux = *queue;
                do
                {
                    if(aux == elem)
                    {
                        element = 1;
                        break;
                    }
                    aux = aux->next;
                    if(aux == elem)
                    {
                        element = 1;
                        break;
                    }


                }while(aux->next != *queue);
                if(element)
                {
                    queue_t *ultimo, *proximo;
                    ultimo = elem->prev;
                    proximo = elem->next;
                    ultimo->next = proximo;
                    proximo->prev = ultimo;
                    if(*queue == elem)
                    {

                        *queue = proximo;
                        if(*queue == elem)
                            *queue = NULL;
                    }

                    elem->prev = NULL;
                    elem->next = NULL;

                    return elem;
                }
            }
            else
            {
                perror ("Erro no elemento");
                return NULL;
            }
        }
        else
        {
            perror ("Fila vazia");
            return NULL;
        }
    }
    else
    {
        perror ("Erro na fila");
        return NULL;
    }
}

//------------------------------------------------------------------------------
// Conta o numero de elementos na fila
// Retorno: numero de elementos na fila

int queue_size (queue_t *queue)
{

    if(queue == NULL)
    {
        return 0;
    }

    queue_t *aux = queue;;
    int cont = 1;
    while(aux->next != queue)
    {

        cont++;
        aux = aux->next;
    }
    return cont;
}

//------------------------------------------------------------------------------
// Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
// elemento é feita por uma função externa, definida pelo programa que
// usa a biblioteca. Essa função deve ter o seguinte protótipo:
//
// void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir

void queue_print (char *name, queue_t *queue, void print_elem (void*) )
{
    if(queue)
    {
        queue_t *aux = queue;
        do
        {
            print_elem(aux);
            aux = aux->next;
        }while(aux != queue);
    }
}



