#include "pingpong.h"
#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

// operating system check
#if defined(_WIN32) || (!defined(__unix__) && !defined(__unix) && (!defined(__APPLE__) || !defined(__MACH__)))
#warning Este codigo foi planejado para ambientes UNIX (LInux, *BSD, MacOS). A compilacao e execucao em outros ambientes e responsabilidade do usuario.
#endif

#define STACKSIZE 32768		/* tamanho de pilha das threads */
#define _XOPEN_SOURCE 600	/* para compilar no MacOS */

int id, userTasks;
task_t Main, *Atual, Dispatcher;
task_t *Fila = NULL;
// funções gerais ==============================================================

task_t* scheduler()
{
    task_t *aux = Fila;

    queue_remove((queue_t**) &Fila, (queue_t*) aux);

    queue_append((queue_t**) &Fila, (queue_t*) aux);
                    printf("oi \n");
    return aux;
}

void dispatcher_body()
{

    while (userTasks > 1)
    {

        task_t* next = scheduler();
        if(next)
        {
            task_switch(next);
        }
    }
    task_exit(&Dispatcher);
}



// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init ()
{
    id = 0;
    userTasks = 0;
    Atual = &Main;

    task_create(&Dispatcher, dispatcher_body, "Dispatcher");

    setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) 			// argumentos para a tarefa
{
    if(task)
    {
        char *stack;
        id++;

        if(getcontext(&task->context))
        {
            perror ("Erro na criacao do contexto");
            return -1;
        }

        stack = malloc (STACKSIZE) ;
        if (stack)
        {
            task->tid = id;
            task->context.uc_stack.ss_sp = stack ; //ponteiro para o comeco da stack
            task->context.uc_stack.ss_size = STACKSIZE; // tamanho alocado para a stack
            task->context.uc_stack.ss_flags = 0; //estado da stack
            task->context.uc_link = 0; // ponteiro para o centexto que sera usado caso esse contexto retorne
        }
        else
        {
            perror ("Erro na criação da pilha");
            return -1;
        }

        makecontext(&task->context, (void*)start_func, 1, arg); // tentei fazer um if aqui mas nao da, exemplos nao utilizam validacao aqui

        if(userTasks > 0)
            queue_append((queue_t**) &Fila, (queue_t*) task);

        userTasks++;

        return task->tid;
    }
    else
    {
        perror ("Erro na tarefa");
        return -1;
    }
}


// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode)
{
    queue_remove((queue_t**)&Fila, (queue_t*) Atual);
    if(userTasks == 1)
        task_switch(&Main);
    else
    {
        userTasks--;
        task_switch(&Dispatcher);
    }

}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task)
{
    if(task)
    {
        task_t *passada;
        passada = Atual;
        Atual = task;
        if(swapcontext(&passada->context, &Atual->context))
        {
            perror ("Erro na troca de contextos");
            return -1;
        }
        else
            return 0;
    }
    else
    {
        perror ("Erro na tarefa");
        return -1;
    }
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return Atual->tid;
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend (task_t *task, task_t **queue) ;

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume (task_t *task) ;

// operações de escalonamento ==================================================

// libera o processador para a próxima tarefa, retornando à fila de tarefas
// prontas ("ready queue")
void task_yield ()
{
    task_switch(&Dispatcher);
}


