// PingPongOS - PingPong Operating System
// Guilherme Hideki && Lucas Rech
// Versão 1.2 -- Abril 2018
//
// Estruturas de dados internas do sistema operacional
#include <ucontext.h>

#ifndef __DATATYPES__
#define __DATATYPES__

// Estrutura que define uma tarefa
typedef struct task_t
{
    struct task_t *prev, *next, *dependencias;
    int tid, static_priority, dinamic_priority, quantum, running_time, activations, exitCode, timer;
    ucontext_t context;
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif



