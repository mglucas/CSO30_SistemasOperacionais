#include "pingpong.h"
#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <signal.h>
#include <sys/time.h>

// operating system check
#if defined(_WIN32) || (!defined(__unix__) && !defined(__unix) && (!defined(__APPLE__) || !defined(__MACH__)))
#warning Este codigo foi planejado para ambientes UNIX (LInux, *BSD, MacOS). A compilacao e execucao em outros ambientes e responsabilidade do usuario.
#endif

#define STACKSIZE 32768		/* tamanho de pilha das threads */
#define _XOPEN_SOURCE 600	/* para compilar no MacOS */
#define TASK_AGING -1

int id, userTasks, tempo;
task_t Main, *Atual, Dispatcher;
task_t *Fila = NULL;
task_t *Suspend = NULL;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

// funções gerais ==============================================================

task_t* scheduler()
{
    task_t *aux = Fila->next;
    task_t *priority = Fila;

    do
    {
        //printf("%d %d // ", aux->tid, aux->dinamic_priority);
        if(priority->dinamic_priority > aux->dinamic_priority)
            priority = aux;
        else if(priority->dinamic_priority == aux->dinamic_priority)
            if(priority->static_priority > aux->static_priority)
                priority = aux;

        aux = aux->next;
    }while(aux != Fila->next);

    aux = Fila;
    do
    {
        aux->dinamic_priority += TASK_AGING;
        aux = aux->next;
    }
    while(aux != Fila);

    task_setprio(priority, task_getprio(priority));

    return priority;
}

void dispatcher_body()
{

    while (userTasks > 1)
    {

        task_t* next = scheduler();
        if(next)
        {
            task_switch(next);
        }
    }
    task_exit(&Dispatcher);
}

// tratador do sinal
void tratador (int signum)
{
    tempo++;
    Atual->running_time++;
    Atual->quantum--;
    if(Atual->quantum == 0)
    {
        Atual->activations++;
        Dispatcher.activations++;
        Atual->quantum = 20;
        Dispatcher.running_time++;
        //printf ("uia em %d ", tempo) ;
        //printf("oi %d", Atual->tid);
        task_yield();
    }

}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init ()
{
    id = 0;
    userTasks = 0;
    Atual = &Main;

        char *stack;

        getcontext(&Main.context);

        stack = malloc (STACKSIZE) ;
        if (stack)
        {
            Main.tid = id;
            Main.static_priority = 0;
            Main.dinamic_priority = 0;
            Main.quantum = 20;
            Main.activations = 1;
            Main.context.uc_stack.ss_sp = stack ; //ponteiro para o comeco da stack
            Main.context.uc_stack.ss_size = STACKSIZE; // tamanho alocado para a stack
            Main.context.uc_stack.ss_flags = 0; //estado da stack
            Main.context.uc_link = 0; // ponteiro para o centexto que sera usado caso esse contexto retorne
        }
        else
        {
            perror ("Erro na criação da pilha");
        }

    queue_append((queue_t**) &Fila, &Main);


    task_create(&Dispatcher, dispatcher_body, "Dispatcher");

    setvbuf(stdout, 0, _IONBF, 0);

    // registra a a��o para o sinal de timer SIGALRM
    action.sa_handler = tratador ;
    sigemptyset (&action.sa_mask) ;
    action.sa_flags = 0 ;
    if (sigaction (SIGALRM, &action, 0) < 0)
    {
        perror ("Erro em sigaction: ") ;
        exit (1) ;
    }

    timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
    timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
    timer.it_interval.tv_usec = 1000 ;   // disparos subsequentes, em micro-segundos
    timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos

    // arma o temporizador ITIMER_REAL (vide man setitimer)
    if (setitimer (ITIMER_REAL, &timer, 0) < 0)
    {
        perror ("Erro em setitimer: ") ;
        exit (1) ;
    }

}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) 			// argumentos para a tarefa
{
    if(task)
    {
        char *stack;
        id++;

        if(getcontext(&task->context))
        {
            perror ("Erro na criacao do contexto");
            return -1;
        }

        stack = malloc (STACKSIZE) ;
        if (stack)
        {
            task->tid = id;
            task->static_priority = 0;
            task->dinamic_priority = 0;
            task->quantum = 20;
            task->activations = 1;
            task->context.uc_stack.ss_sp = stack ; //ponteiro para o comeco da stack
            task->context.uc_stack.ss_size = STACKSIZE; // tamanho alocado para a stack
            task->context.uc_stack.ss_flags = 0; //estado da stack
            task->context.uc_link = 0; // ponteiro para o centexto que sera usado caso esse contexto retorne
        }
        else
        {
            perror ("Erro na criação da pilha");
            return -1;
        }

        makecontext(&task->context, (void*)start_func, 1, arg); // tentei fazer um if aqui mas nao da, exemplos nao utilizam validacao aqui

        if(userTasks > 0)
            queue_append((queue_t**) &Fila, (queue_t*) task);

        userTasks++;

        return task->tid;
    }
    else
    {
        perror ("Erro na tarefa");
        return -1;
    }
}


// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode)
{

    Atual->exitCode = exitCode;

    if(Atual->dependencias)
    {
        task_t *aux = Atual->dependencias;

        do
        {
            aux = aux->next;

            task_t *aux2 = aux;
            task_resume(aux2);


        }  while(aux != Atual->dependencias);
    }

    printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations \n", Atual->tid, systime(), Atual->running_time, Atual->activations );
    queue_remove((queue_t**)&Fila, (queue_t*) Atual);
    if(userTasks == 1)
        task_switch(&Main);
    else
    {
        userTasks--;
        task_switch(&Dispatcher);
    }

}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task)
{
    if(task)
    {
        task_t *passada;
        passada = Atual;
        Atual = task;
        if(swapcontext(&passada->context, &Atual->context))
        {
            perror ("Erro na troca de contextos");
            return -1;
        }
        else
            return 0;
    }
    else
    {
        perror ("Erro na tarefa");
        return -1;
    }
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id ()
{
    return Atual->tid;
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend (task_t *task, task_t **queue)
{
    printf("oi");
    if(task)
    {

        queue_remove((queue_t**)&Fila, (queue_t*) task);
        task->next = NULL;
        task->prev = NULL;

        queue_append((queue_t**) queue, (queue_t*) task);
    }
    else
    {

        queue_remove((queue_t**)&Fila, (queue_t*) Atual);

        Atual->next = NULL;
        Atual->prev = NULL;
        queue_append((queue_t**) queue, (queue_t*) Atual);
    }

    task_yield();
}

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume (task_t *task)
{
    task->next = NULL;
    task->prev = NULL;
    queue_append((queue_t**) &Fila, (queue_t*) task);

    printf("ha");
}

// operações de escalonamento ==================================================

// libera o processador para a próxima tarefa, retornando à fila de tarefas
// prontas ("ready queue")
void task_yield ()
{
    task_switch(&Dispatcher);
}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio)
{
    if(prio < -20)
        prio = -20;
    else if(prio > 20)
        prio = 20;

    if(task)
    {
        task->static_priority = prio;
        task->dinamic_priority = prio;
    }
    else
    {
        Atual->static_priority = prio;
        Atual->dinamic_priority = prio;
    }
}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task)
{
    if(task)
        return task->static_priority;
    else
        return Atual->dinamic_priority;
}

// retorna o relógio atual (em milisegundos)
unsigned int systime ()
{
    return tempo;
}

// operações de sincronização ==================================================

// a tarefa corrente aguarda o encerramento de outra task
int task_join (task_t *task)
{

    task_t *aux = Fila->next;

    do
    {
        if(task == aux)
        {
            task_suspend(NULL, &task->dependencias);
            return(task->exitCode);
        }
        aux = aux->next;
    }while(aux != Fila->next);

    return -1;
}


