#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"

//Tarefas que serão alteradas
task_t      p1, p2, p3, c1, c2, c3;

//valor inteiro entre 0 e 99
int item;

//fila de inteiros com capacidade para até 5 elementos, inicialmente vazia, acessada com política
//FIFO. Pode ser implementado usando um vetor de inteiros ou a biblioteca de filas já desenvolvida.
int buffer[5];

//Variável que será usada para determinar qual foi a última posição para colocar o item e o último produto consumido
int vaga=0;

//Semáforos
semaphore_t s_buffer, s_item, s_vaga ;

//Função responsável por printar o conteúdo do buffer
void printBuffer(){
    for(int i = 0; i<5; i++){
        printf("buffer[%d]=%d\n", i, buffer[i]);
    }

    printf("\n");
}

void produtor(void * arg){

    while(1){

        task_sleep(1);

        //determinad um item entre 1 - 99
        item = rand() % 100;

        //Verifica se os semáforos estão disponíveis
        sem_down(&s_vaga);
        sem_down(&s_buffer);

        //insere o item no buffer
        buffer[vaga] = item;

        //printa item produzido
        printf(" %s produziu %d\n", (char *) arg, item);
        
        //printBuffer();

        //incrementa a vaga        
        vaga++;

        //Zera a vaga que deve ser incrementada
        if(vaga >= 5){
            vaga = 0;            
        }

        //Libera semáforos do buffer e do item
        sem_up(&s_buffer);
        sem_up(&s_item);
        
    }

}

void consumidor(void * arg){

    while(1){
        //Verifica se os semáforos estão disponíveis
        sem_down(&s_item);
        sem_down(&s_buffer);

        int itemConsumido;
        //Retira item do buffer
        for(int i = 0; i<5;i++){
            if(buffer[i] != 0){ 
                itemConsumido = buffer[i];
                
                //Zera a posição do buffer
                buffer[i]=0;

                //decrementa a vaga
                vaga--;

                break;
            }
        }
        //printa item consumido
        printf(" %s consumiu %d \n", (char *) arg, itemConsumido); 

        //printBuffer();

        //Libera os Semáforos
        sem_up(&s_buffer);
        sem_up(&s_vaga);
        
        task_sleep(1);
    }
}

int main(int argc, char *argv[]){

    //Zerando o buffer
    for(int i=0;i<5;i++){
        buffer[i] = 0;
    }

   printf ("Main INICIO\n") ;

    pingpong_init () ;

    //Inicialização dos Semáforos
    sem_create (&s_buffer, 1);
    sem_create (&s_item, 0);
    sem_create (&s_vaga, 5);

    //Ocupa o Buffer até que seja produzido alguma coisa
    //printBuffer();
    
    //Cria os produtores
    task_create (&p1, produtor, "P1 ") ;
    task_create (&p2, produtor, "P2 ") ;
    task_create (&p3, produtor, "P3 ") ;

    //Cria os consumidores
    task_create (&c1, consumidor, "                 C1 ") ;
    task_create (&c2, consumidor, "                 C2 ") ;
    task_create (&c3, consumidor, "                 C3 ") ;
    

    //Destroi os Semáforos    
    sem_destroy (&s_buffer);
    sem_destroy (&s_item);
    sem_destroy (&s_vaga);

    printf ("Main FIM\n") ;
    task_exit (0) ;

    exit(0);

}
