#include "pingpong.h"
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include <sys/time.h>
#include <signal.h>

#define STACKSIZE 32768		/* tamanho de pilha das threads */
int ID_TASKS = 0;

task_t* TaskAtual = NULL;
task_t* TaskMain = NULL;
task_t* TaskDispatcher = NULL;

task_t* Tasks;
task_t* Ready_queue;

int Relogio = 0;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;


// Task_yield - Faz o switch para o dispatcher
void task_yield(){
    task_switch(TaskDispatcher);    
};

//Função que determina qual a próxima tarefa à ser executada
task_t* scheduler(){
    //Retorna a próxima tarefa ao lado do dispatcher, que será a próxima a ser executada
    //Utiliza o padrão FIFO
    if(TaskDispatcher->next != TaskMain){
        return TaskDispatcher->next;

    }
    //Se o next do Dispatcher for a Main, retorna NULL
    return NULL;
}

//Função atribuida ao Dispatcher
void dispatcher_body(){
    //Enquanto a Fila de tarefas não for vazia
    
    //Ponteiro que armazena a próxima tarefa a ser executada, enviada pelo scheduler
    task_t* next;

    while( (queue_size((queue_t*)Tasks) - 2) > 0){
        //variável que armazena o tamanho atual da fila para futura comparação
        int tamanho = queue_size((queue_t*)Tasks);

        //Atribui próxima tarefa ao next
        next = scheduler();

        if(next){
            //Faz o switch para a próxima tarefa
            task_switch(next);

            //Se o tamanho da fila não se alterar durante a execução da tarefa
            //significa que ela ainda não foi finalizada então é preciso colocá-la novamente no final da fila de execuções
            if( queue_size((queue_t*)Tasks) == tamanho){
                queue_remove((queue_t **)&Tasks, (queue_t *)next);
                queue_append((queue_t **)&Tasks, (queue_t *)next);
            }
        }
    }

    task_exit(0);
}

void tratador (int signum)
{
  //printf ("Recebi o sinal %d\n", signum) ;
  TaskAtual->Ticks--;

  Relogio++;
  TaskAtual->processor_time++;

  if(TaskAtual->Ticks==0){
        TaskAtual->Ticks=20;
        task_yield();
        
  }

}


// Funções de Inicialização do Sistema  ==================================================

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init (){
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0);

    //Inicializa a main com o contexto atual executado
    TaskMain= malloc(sizeof(task_t));
    getcontext(&(TaskMain->context));
    TaskMain->tid = ID_TASKS++;    
    
    TaskMain->next = NULL;
    TaskMain->prev = NULL;
    
    queue_append((queue_t**)&Tasks, (queue_t*)TaskMain);
    //printf("TaskMain: %p \n",TaskMain);    
    //printf("TaskMain->next: %p \n",TaskMain->next);
    //printf("TaskMain->prev: %p \n",TaskMain->prev);
    
    //Atribui à tarefa atual à main:
    TaskAtual = TaskMain;

    //Cria o Dispatcher
    TaskDispatcher = malloc(sizeof(task_t));
    task_create(TaskDispatcher, dispatcher_body, 0);

    //printf("TaskMain: %p \n",TaskMain);    
    // printf("TaskMain->next: %p \n",TaskMain->next);
    // printf("TaskMain->prev: %p \n",TaskMain->prev);
    
    //printf("TaskDispatcher: %p \n",TaskDispatcher);    
    // printf("TaskDispatcher->next: %p \n",TaskDispatcher->next);
    // printf("TaskDispatcher->prev: %p \n",TaskDispatcher->prev);

    // registra a ação para o sinal de timer SIGALRM
      action.sa_handler = tratador ;
      sigemptyset (&action.sa_mask) ;
      action.sa_flags = 0 ;

      if (sigaction (SIGALRM, &action, 0) < 0)
      {
        perror ("Erro em sigaction: ") ;
        exit (1) ;
      }

      // ajusta valores do temporizador
      // Ajustado para 1 ms

      timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
      timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
      timer.it_interval.tv_usec =  1000 ;   // disparos subsequentes, em micro-segundos
      timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos


          // arma o temporizador ITIMER_REAL (vide man setitimer)
      if (setitimer (ITIMER_REAL, &timer, 0) < 0)
      {
        perror ("Erro em setitimer: ") ;
        exit (1) ;
      }
    
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg){			// argumentos para a tarefa

	// Criando o contexto para a tarefa desejada.
	getcontext(&(task->context));

	char *stack;
	stack = malloc (STACKSIZE) ;
	   if (stack)
	   {
	      task->context.uc_stack.ss_sp = stack ;
	      task->context.uc_stack.ss_size = STACKSIZE;
	      task->context.uc_stack.ss_flags = 0;
	      task->context.uc_link = 0;
	   }
	   else
	   {
	      perror ("Erro na criação da pilha: ");
	      exit (1);
	   }

	//Atribuindo à tarefa, suas respectivas funções e argumentos
	makecontext (&(task->context),(void *)(*start_func),1 ,arg);

	task->tid = ID_TASKS;
    task->Ticks = 20;

    //Seta o tempo de execução que a tarefa iniciou
    task->time_init = systime();
    task->time_exit = 0;
    task->processor_time = 0;
    task->activations = 0;

    queue_append((queue_t**)&Tasks, (queue_t*) task);
    
    ID_TASKS++;
    
	//Essa função retorna o ID, mas ainda não atribuímos valores aos ID's
	return(task->tid);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode){

    task_t* aux;
    aux = TaskAtual;

    //Seta o tempo de execução que a tarefa iniciou
    TaskAtual->time_exit = systime();

    queue_remove((queue_t**)&Tasks, (queue_t*) aux);
    printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", TaskAtual->tid, (TaskAtual->time_exit - TaskAtual->time_init ), TaskAtual->processor_time, TaskAtual->activations);

    if(TaskAtual->tid == 1){ 
        //Quando estou no contexto de ID = 1 estou no Dispatcher e volto pra Main
        task_switch(TaskMain);    
    }
    else{
        //Quando estou em contextos de ID != 1 retorno para o Dispatcher
        task_switch(TaskDispatcher);
        //task_switch(TaskMain);
    }

};

// alterna a execução para a tarefa indicada
int task_switch (task_t *task){
    
    //printf("Estou no switch\n");

    //Incremento do Task Activations    
    task->activations++;

    task_t* anterior;
    anterior = TaskAtual;
    TaskAtual = task;

    int result = swapcontext(&(anterior->context), &(task->context));
    //printf("%d", result);

    if (result == -1){
      return -1;
    }
    else{
        return 0;
    }
    
};

// retorna o identificador da tarefa corrente (main eh 0)
int task_id (){
    return TaskAtual->tid;
};

void task_suspend (task_t *task, task_t **queue){
	// Para suspender a tarefa, removemos ela da sua fila atual
	// Adicionando a fila "queue"
	// Mudando seu estado para "suspensa"
	// Se a task == NULL, consideramos a tarefa corrente. Se queue for NULL, não remove a tarefa.

	queue_t *TaskToSuspend;

	// Se a fila for nula, não retira a task da fila atual
	if (queue==NULL){
		return;
	}
	
	// Se a task e NULL, coloca a TaskAtual na queue
	// Senão, coloca a task na queue
	if (task == NULL){
		// Remove a tarefa de sua fila atual
		TaskToSuspend = queue_remove((queue_t**)&Tasks, (queue_t*) TaskAtual);
		
		//Adiciona a tarefa a queue
		queue_append((queue_t**)&queue, TaskToSuspend);

		// Retorna ao dispatcher
		task_yield();
	}
	else{

		// Remove a tarefa de sua fila atual
		TaskToSuspend = queue_remove((queue_t**)&Tasks, (queue_t*) task);
		
		//Adiciona a tarefa a queue
		queue_append((queue_t**)&queue, TaskToSuspend);

		// Retorna ao dispatcher
		task_yield();
	}

};

void task_resume (task_t *task){
	// Acorda uma tarefa, removendo ela de sua fila atual (Se estiver em uma fila)
	// Adiciona ela a fila de prontas (ready_queue)

	// MUDANDO SEU ESTADO PARA PRONTA

	queue_t *TaskToResume;

	TaskToResume = queue_remove((queue_t**)&Tasks, (queue_t*) task);

	// Se o retorno da remoção e NULL, a tarefa não está em uma fila
	if (TaskToResume == NULL){
		printf("Não está em uma fila, não pode ser removida\n");
		// Retorna a tarefa a fila que pertencia
		queue_append((queue_t**)&Tasks, TaskToResume);
	}
	else
		// Removida a tarefa da fila atual, aloca ela à fila de prontas (ready_queue)
		queue_append((queue_t**)&Ready_queue, TaskToResume);

};

// operações de gestão do tempo ================================================

// suspende a tarefa corrente por t segundos
void task_sleep (int t){

};

// retorna o relógio atual (em milisegundos)
unsigned int systime (){
    return Relogio;
};

