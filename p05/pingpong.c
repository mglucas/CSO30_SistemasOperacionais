#include "pingpong.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */
int ID_TASKS = 0;

task_t* TaskAtual = NULL;
task_t* TaskMain = NULL;
task_t* TaskDispatcher = NULL;

task_t* Tasks;
task_t* Ready_queue;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

// operações de escalonamento ==================================================

// Task_yield - Faz o switch para o dispatcher
void task_yield (){
    task_switch(TaskDispatcher);
};


// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
    if(task){
        task->prio_est = prio;
        task->old = task->prio_est;
    }
    else{
        TaskAtual->prio_est = prio;
        TaskAtual->old = TaskAtual->prio_est;
    }
    //printf("Prioridade da Tarefa %d: %d\n", task->tid, task->prio);

};

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
    if(task){
        return task->prio_est;
    }
    else
        return TaskAtual->prio_est;
};

// operações do Dispatcher ==================================================

//Função que determina qual a próxima tarefa à ser executada - Escalonador
//Implementa o escalonador por prioridade
task_t* scheduler(){
    //Verifica qual é a menor prioridade entre as tarefas
    task_t* next_task = NULL;
    task_t* aux = TaskDispatcher->next;

    //O nosso sistema de prioridade está na escala de -20 à 20
    int prio_min = 21;        //Recebe uma prioridade alta

    //printf("Estou no scheduler\n");
    if(TaskDispatcher->next == TaskMain){
    //Se o next do Dispatcher for a Main, retorna NULL
        printf("Próxima tarefa é a Main\n");
        return NULL;
    }
    else{
    //Verifica toda a lista de Tarefas - excluindo Main e Dispatcher
        while(aux != TaskMain){
            if(aux->old < prio_min){
                prio_min = aux->old;

                //Se outro next foi selecionado antes, mas não é o menor, então decremente também seu old
                if(next_task){
                    next_task->old-=1;
                }

                next_task = aux;
            }
            else{
                aux->old-=1;
            }

            aux = aux->next;
        }

        //Retorna o que tem menor prioridade - que será executado primeiro
        //printf("retorno: %p\n", next_task);
        next_task->old = next_task->prio_est;
        return next_task;
    }
}

//Função atribuida ao Dispatcher
void dispatcher_body(){
    //Ponteiro que armazena a próxima tarefa a ser executada, enviada pelo scheduler
    task_t* next;

    //Enquanto a Fila de tarefas não for vazia
    while( (queue_size((queue_t*)Tasks) - 2) > 0){

        //Atribui próxima tarefa ao next
        next = scheduler();
        //printf("Next-Task: %p\n", next);


        if(next){
            //Faz o switch para a próxima tarefa
            task_switch(next);
        }
    }

    task_exit(0);
}

// Decrementa os Ticks a cada execução
void tratador (int signum)
{
  //printf ("Recebi o sinal %d\n", signum) ;
  TaskAtual->Ticks--;

// Se os 20 Ticks não forem suficientes
// Realoca 20 Ticks para a tarefa e faz a troca
  if(TaskAtual->Ticks==1){
        TaskAtual->Ticks=20;
        task_yield();
  }

}

// Funções de Inicialização do Sistema  ==================================================
// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init (){
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0);

    //Inicializa a main com o contexto atual executado
    TaskMain= malloc(sizeof(task_t));
    getcontext(&(TaskMain->context));
    TaskMain->tid = ID_TASKS++;

    TaskMain->next = NULL;
    TaskMain->prev = NULL;

    queue_append((queue_t**)&Tasks, (queue_t*)TaskMain);
    //printf("TaskMain: %p \n",TaskMain);
    //printf("TaskMain->next: %p \n",TaskMain->next);
    //printf("TaskMain->prev: %p \n",TaskMain->prev);

    //Atribui à tarefa atual à main:
    TaskAtual = TaskMain;

    //Cria o Dispatcher
    TaskDispatcher = malloc(sizeof(task_t));
    task_create(TaskDispatcher, dispatcher_body, 0);


    // Inicialização do timer de 1ms

  // registra a ação para o sinal de timer SIGALRM
  action.sa_handler = tratador ;
  sigemptyset (&action.sa_mask) ;
  action.sa_flags = 0 ;

  if (sigaction (SIGALRM, &action, 0) < 0)
  {
    perror ("Erro em sigaction: ") ;
    exit (1) ;
  }

  // ajusta valores do temporizador
  // Ajustado para 1 ms

  timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
  timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
  timer.it_interval.tv_usec =  1000 ;   // disparos subsequentes, em micro-segundos
  timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos


      // arma o temporizador ITIMER_REAL (vide man setitimer)
  if (setitimer (ITIMER_REAL, &timer, 0) < 0)
  {
    perror ("Erro em setitimer: ") ;
    exit (1) ;
  }
    //printf("TaskMain: %p \n",TaskMain);
    // printf("TaskMain->next: %p \n",TaskMain->next);
    // printf("TaskMain->prev: %p \n",TaskMain->prev);

    //printf("TaskDispatcher: %p \n",TaskDispatcher);
    // printf("TaskDispatcher->next: %p \n",TaskDispatcher->next);
    // printf("TaskDispatcher->prev: %p \n",TaskDispatcher->prev);

}


// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg){			// argumentos para a tarefa

	// Criando o contexto para a tarefa desejada.
	getcontext(&(task->context));

	char *stack;
	stack = malloc (STACKSIZE) ;
	   if (stack)
	   {
	      task->context.uc_stack.ss_sp = stack ;
	      task->context.uc_stack.ss_size = STACKSIZE;
	      task->context.uc_stack.ss_flags = 0;
	      task->context.uc_link = 0;
	   }
	   else
	   {
	      perror ("Erro na criação da pilha: ");
	      exit (1);
	   }

	//Atribuindo à tarefa, suas respectivas funções e argumentos
	makecontext (&(task->context),(void *)(*start_func),1 ,arg);

	task->tid = ID_TASKS;
    task->Ticks = 20;
    //printf("Tarefa %d criada\n", task->tid);
    //printf("Task: %p \n",task);

    //Ao ser criada, cada tarefa recebe a prioridade default (0).
    task->old = 0;
    task->prio_est = 0;

    queue_append((queue_t**)&Tasks, (queue_t*) task);

    ID_TASKS++;

	//Essa função retorna o ID, mas ainda não atribuímos valores aos ID's
	return(task->tid);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode){

    task_t* aux;
    aux = TaskAtual;

    queue_remove((queue_t**)&Tasks, (queue_t*) aux);

    if(TaskAtual->tid == 1){
        //Quando estou no contexto de ID = 1 estou no Dispatcher e volto pra Main
        task_switch(TaskMain);
    }
    else{
        //Quando estou em contextos de ID != 1 retorno para o Dispatcher
        task_switch(TaskDispatcher);
        //task_switch(TaskMain);
    }

};

// alterna a execução para a tarefa indicada
int task_switch (task_t *task){
    //printf("Estou no switch\n");
    task_t* anterior;
    anterior = TaskAtual;
    TaskAtual = task;

    int result = swapcontext(&(anterior->context), &(task->context));
    //printf("%d", result);

    if (result == -1){
      return -1;
    }
    else{
        return 0;
    }

};

// retorna o identificador da tarefa corrente (main eh 0)
int task_id (){
    return TaskAtual->tid;
};

void task_suspend (task_t *task, task_t **queue){
	// Para suspender a tarefa, removemos ela da sua fila atual
	// Adicionando a fila "queue"
	// Mudando seu estado para "suspensa"
	// Se a task == NULL, consideramos a tarefa corrente. Se queue for NULL, não remove a tarefa.[

	// MUDAR O ESTADO DA TAREFA???????????

	queue_t *TaskToSuspend;

	// Se a fila for nula, não retira a task da fila atual
	if (queue==NULL){
		return;
	}

	// Se a task e NULL, coloca a TaskAtual na queue
	// Senão, coloca a task na queue
	if (task == NULL){
		// Remove a tarefa de sua fila atual
		TaskToSuspend = queue_remove((queue_t**)&Tasks, (queue_t*) TaskAtual);

		//Adiciona a tarefa a queue
		queue_append((queue_t**)&queue, TaskToSuspend);

		// Retorna ao dispatcher
		task_yield();
	}
	else{

		// Remove a tarefa de sua fila atual
		TaskToSuspend = queue_remove((queue_t**)&Tasks, (queue_t*) task);

		//Adiciona a tarefa a queue
		queue_append((queue_t**)&queue, TaskToSuspend);

		// Retorna ao dispatcher
		task_yield();
	}

};

void task_resume (task_t *task){
	// Acorda uma tarefa, removendo ela de sua fila atual (Se estiver em uma fila)
	// Adiciona ela a fila de prontas (ready_queue)

	// MUDANDO SEU ESTADO PARA PRONTA

	queue_t *TaskToResume;

	TaskToResume = queue_remove((queue_t**)&Tasks, (queue_t*) task);

	// Se o retorno da remoção e NULL, a tarefa não está em uma fila
	if (TaskToResume == NULL){
		//printf("Não está em uma fila, não pode ser removida\n");
		// Retorna a tarefa a fila que pertencia
		queue_append((queue_t**)&Tasks, TaskToResume);
	}
	else
		// Removida a tarefa da fila atual, aloca ela à fila de prontas (ready_queue)
		queue_append((queue_t**)&Ready_queue, TaskToResume);



};
