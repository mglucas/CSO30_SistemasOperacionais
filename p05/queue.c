#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

#define b 3

void queue_append (queue_t **queue, queue_t *elem){
	//Verificando condições de erro
	// A fila deve existir
	if(queue == NULL)
		printf("Fila não existe \n");
	// O elemento deve existir
	else if(elem == NULL)
		printf("Elemento nao existe  \n");	
	// O elemento não deve estar em outra fila
	else if(!(elem->next==NULL) && !(elem->prev==NULL))
		printf("Elemento esta em outra fila \n");
	// Insere o PRIMEIRO elemento na fila
	else if(*queue == NULL){
		(*queue) = elem;
		(*queue)->next = elem;
		(*queue)->prev = elem;
		
		}
	// Insere os demais elementos na fila
	else {
		queue_t *aux = *queue; //Guardando o primeiro elemento da fila
								// e suas conexoes

		while (aux->next != *queue)
			aux=aux->next; //Define o último da fila

		elem->prev = aux; // Define o último da fila como o elemento
						// O elem->prev recebe o antigo ultimo
 
		aux->next = elem; // Define o penultimo->next como elemento

		(*queue)->prev=elem; // A cabeça da fila ganha o prev como elem

		elem->next=(*queue); // Elemento aponta para o início da fila
	
		}
};

queue_t *queue_remove (queue_t **queue, queue_t *elem){
	
	//Verificando condições de erro - Retorno NULL
	
	// A fila deve existir
	if(queue == NULL){
		printf("Fila não existe \n");
		return NULL;
	}
	// A fila não deve estar vazia
	else if(*queue == NULL){
		printf("Fila vazia!\n");
		return NULL;
	}
	// O elemento deve existir
	else if(elem == NULL){
		printf("Elemento nao existe  \n");
		return NULL;
	}
	//Caso 0 - com apenas 1 elemento na fila
	else if(elem == *queue && (*queue)->next == (*queue)->prev && *queue == (*queue)->next ){

		//Guarda o primeiro elemento da Fila
		queue_t *elem_removido = (*queue);

		//Desvincula o elemento da fila
		elem_removido->next = NULL;
		elem_removido->prev = NULL;

		//Retira o elemento da cabeça da fila
		*queue = NULL;

		//Retorna o elemento
		return elem_removido;

	}
	//Caso 1 - Remoção do primeiro elemento
	else if(elem == *queue){


		//Guarda os dois Elementos prev e next
		queue_t *aux = *queue;

		while (aux->next != *queue)
			aux=aux->next; //Define o último da fila


		//Vincula os dois elementos eliminando o elem do meio
		aux->next = elem->next;
		*queue = elem->next;
		elem->next->prev = aux;

		queue_t *elem_removido = elem;

		elem = *queue;

		//Retira o Vinculo do elemento com a fila
		elem_removido->prev = NULL;
		elem_removido->next = NULL;

		//Retorna o elemento
		return elem_removido;

	}
	//Caso Geral - Com mais de 2 elementos na fila
	else{

        queue_t *aux = *queue;

        //Encontra o Elemento
        while (aux->next != *queue &&  aux != elem)
			aux=aux->next; //Define o último da fila

        if( elem != aux){
            printf("Elemento não está na fila\n");
            return NULL;
        }

		//Guarda os dois Elementos prev e next
		queue_t *prev = elem->prev;
		queue_t *next = elem->next;

		//Vincula os dois elementos eliminando o elem do meio
		prev->next = next;
		next->prev = prev;


		//Retira o Vinculo do elemento com a fila
		elem->prev = NULL;
		elem->next = NULL;

		//Retorna o elemento
		return elem;
	}

};

int queue_size (queue_t *queue){
	// Conta o numero de elementos da fila
	// retorna o numero de elementos da fila	
	int tam=1;
	queue_t *aux=queue;

	if(queue == NULL)
		return 0;
	else{
		while(aux->next != queue){
			aux=aux->next;
			tam++;
		}
	}

	return tam;
};

void queue_print (char *name, queue_t *queue, void print_elem (void*) ){
	
    queue_t *print_aux = queue;

    
    // A fila está Vazia
	if(queue == NULL){
		printf("Fila Vazia \n");
	}
    else{
	    while(print_aux->next != queue){
			    print_elem(print_aux);
			    print_aux = print_aux->next;
	    }
    }
};

