#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

// operating system check
#if defined(_WIN32) || (!defined(__unix__) && !defined(__unix) && (!defined(__APPLE__) || !defined(__MACH__)))
#warning Este codigo foi planejado para ambientes UNIX (LInux, *BSD, MacOS). A compilacao e execucao em outros ambientes e responsabilidade do usuario.
#endif

#define STACKSIZE 32768		/* tamanho de pilha das threads */
#define _XOPEN_SOURCE 600	/* para compilar no MacOS */

ucontext_t ContextPing, ContextPong, ContextMain;

/*****************************************************/

void BodyPing (void * arg)
{
   int i ;

   printf ("%s iniciada\n", (char *) arg) ;
   
   for (i=0; i<4; i++)
   {
      printf ("%s %d\n", (char *) arg, i) ;
      swapcontext (&ContextPing, &ContextPong);
   }	
   printf ("%s FIM\n", (char *) arg) ;

   swapcontext (&ContextPing, &ContextMain) ;
}

/*****************************************************/

void BodyPong (void * arg)
{
   int i ;

   printf ("%s iniciada\n", (char *) arg) ;

   for (i=0; i<4; i++)
   {
      printf ("%s %d\n", (char *) arg, i) ;
      swapcontext (&ContextPong, &ContextPing);
   }
   printf ("%s FIM\n", (char *) arg) ;

   swapcontext (&ContextPong, &ContextMain) ;
}

/*****************************************************/

int main (int argc, char *argv[])
{
   char *stack ;

   printf ("Main INICIO\n");

   // Inicializando um contexto na vari�vel ContextPing
   getcontext (&ContextPing);

	// Pilha de tamanho 32768 - Pilha de threads
   stack = malloc (STACKSIZE) ; 
   // Se a pilha n�o � vazia, entra na condicional
   // Ajusta todos os valores internos nas estruturas
   if (stack)
   {
      ContextPing.uc_stack.ss_sp = stack ;
      ContextPing.uc_stack.ss_size = STACKSIZE;
      ContextPing.uc_stack.ss_flags = 0;
      ContextPing.uc_link = 0;
   }
   else
   {
      perror ("Erro na cria��o da pilha: ");
      exit (1);
   }

   // Ajusta par�metros internos do contexto
   // Quando makecontext for chamada, executar� Bodyping com 1 par�metro
   makecontext (&ContextPing, (void*)(*BodyPing), 1, "    Ping");

   // Inicializa um contexto com a vari�vel ContextPong 
   getcontext (&ContextPong);

   stack = malloc (STACKSIZE) ;
   if (stack)
   {
      ContextPong.uc_stack.ss_sp = stack ;
      ContextPong.uc_stack.ss_size = STACKSIZE;
      ContextPong.uc_stack.ss_flags = 0;
      ContextPong.uc_link = 0;
   }
   else
   {
      perror ("Erro na cria��o da pilha: ");
      exit (1);
   }
	
	// Ajusta par�metros internos do contexto
   // Quando makecontext for chamada, executar� Bodyping com 1 par�metro

   makecontext (&ContextPong, (void*)(*BodyPong), 1, "        Pong");

   swapcontext (&ContextMain, &ContextPing);
   swapcontext (&ContextMain, &ContextPong);

   printf ("Main FIM\n");

   exit (0);
}
