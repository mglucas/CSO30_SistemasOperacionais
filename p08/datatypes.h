// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional

#ifndef __DATATYPES__
#define __DATATYPES__
#include <ucontext.h>


// Estrutura que define uma tarefa
typedef struct task_t
{
	struct task_t *prev, *next ; // Para usar filas 
	int tid; 					 // Task_id
	ucontext_t context;			 // Utilizado para contexto
	void *stack;				 // Pilha interna da memória
	struct task_t *parent; 		 // Tarefa pai
    int old;					 // Prioridade estática
    int prio_est;				 // Prioridade Dinâmica
    int Ticks;					 // Quantuns
	// enum status_t status;		
	// preencher quando necessário
    int time_init;				 // Timestamp de início da tarefa
    int time_exit;				 // Timestamp de fim da tarefa
    int processor_time;			 // Timestamp do processador
    int activations;			 // Número de ativações da tarefa

  //Fila de Tarefas que dependem do encerramento para serem acordadas
  struct task_t *Suspensas;
  int estado; // 0 = Ativa ; 1 = Suspensa
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif
