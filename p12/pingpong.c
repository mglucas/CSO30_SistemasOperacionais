#include "pingpong.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include "queue.h"
#include <strings.h>

#define STACKSIZE 32768		/* tamanho de pilha das threads */
int ID_TASKS = 0;

task_t* TaskAtual = NULL;
task_t* depend_aux = NULL;
task_t* TaskMain = NULL;
task_t* TaskDispatcher = NULL;

int Barreira = 0; //Identifica que se ainda possuem Barreiras preenchidas esperando
                    // 0 = Não tem elementos na Barreira; 1 = Existem elementos esperando

task_t* Tasks; // Fila da prontas
task_t* sleep_queue; // Fila da adormecidas

int Exit_Code_Join = 3;  //Código de Erro de uma tarefa Join

int Relogio = 0;

// Controle de preempção
int Preempcao_ativa = 1;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

// operações de escalonamento ==================================================

// Task_yield - Faz o switch para o dispatcher
void task_yield (){
    if(Preempcao_ativa)
        task_switch(TaskDispatcher);
    else
        return;
};


// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
    if(task){
        task->prio_est = prio;
        task->old = task->prio_est;
    }
    else{
        TaskAtual->prio_est = prio;
        TaskAtual->old = TaskAtual->prio_est;
    }
    //printf("Prioridade da Tarefa %d: %d\n", task->tid, task->prio);
};

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
    if(task){
        return task->prio_est;
    }
    else
        return TaskAtual->prio_est;
};

// operações do Dispatcher ==================================================

//Função que determina qual a próxima tarefa à ser executada - Escalonador
//Implementa o escalonador por prioridade
task_t* scheduler(){
    //Verifica qual é a menor prioridade entre as tarefas
    task_t* next_task = NULL;
    task_t* aux = TaskDispatcher->next;

    //O nosso sistema de prioridade está na escala de -20 à 20
    int prio_min = 21;        //Recebe uma prioridade alta

    //printf("Estou no scheduler\n");
    if(TaskDispatcher->next == TaskDispatcher){
    //Se o next do Dispatcher for o Dispatcher, retorna NULL
        // printf("Próxima tarefa é a Dispatcher\n");
        return NULL;
    }
    else{
    //Verifica toda a lista de Tarefas - excluindo Dispatcher
        while(aux != TaskDispatcher){
            if(aux->old < prio_min){
                prio_min = aux->old;

                //Se outro next foi selecionado antes, mas não é o menor,
                //então decremente também seu old
                if(next_task){
                    next_task->old-=1;
                }

                next_task = aux;
            }
            else{
                aux->old-=1;
            }

            aux = aux->next;
        }

        //Retorna o que tem menor prioridade - que será executado primeiro
        //printf("next: %p\n", next_task);
        next_task->old = next_task->prio_est;
        return next_task;
    }
}

//Função atribuida ao Dispatcher
void dispatcher_body(){
    //Ponteiro que armazena a próxima tarefa a ser executada, enviada pelo scheduler
    task_t* next;

    //Enquanto a Fila de tarefas Prontas não for vazia
    //E a Fila de Adormecidas não for vazia
    while( (queue_size((queue_t*)Tasks) - 1) > 0 || (queue_size((queue_t*)sleep_queue) > 0) || Barreira == 1 ){

        task_t* aux = sleep_queue;
        if(queue_size((queue_t*)sleep_queue) > 0){
            //printf("Tamanho Sleep queue: %d\n",queue_size((queue_t*)sleep_queue));
            int tamanho = queue_size((queue_t*)sleep_queue);
            for(int i=0; i<tamanho; i++){
                //printf("Acordar: %d; id: %d; Relógio: %d\n", aux->acorda, aux->tid, systime());
                if(aux->acorda < systime()){
                    task_resume(aux);
                    aux = aux->next;
                    break;

                }
                else{
                    aux = aux->next;
                }
            }
        }

        //Atribui próxima tarefa ao next
        next = scheduler();
        //printf("Next-Task: %p\n", next);

        if(next){
            //Faz o switch para a próxima tarefa
            task_switch(next);
        }
    }

    task_exit(0);
}

void tratador (int signum){
  //printf ("Recebi o sinal %d\n", signum) ;
  TaskAtual->Ticks--;

  Relogio++;
  TaskAtual->processor_time++;

  if(TaskAtual->Ticks==1){
        TaskAtual->Ticks=20;
        task_yield();

  }

}

// Funções de Inicialização do Sistema  ==================================================
// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init (){
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0);

    //Cria o Dispatcher
    TaskDispatcher = malloc(sizeof(task_t));
    task_create(TaskDispatcher, dispatcher_body, 0);

    //Inicializa a main com o contexto atual executado
    TaskMain= malloc(sizeof(task_t));
    getcontext(&(TaskMain->context));
    TaskMain->tid = ID_TASKS++;

    TaskMain->Ticks = 20;

    //Seta o tempo de execução que a tarefa iniciou
    TaskMain->time_init = systime();
    TaskMain->time_exit = 0;
    TaskMain->processor_time = 0;
    TaskMain->activations = 0;
    //printf("Task: %p \n",task);

    //Ao ser criada, cada tarefa recebe a prioridade default (0).
    TaskMain->old = 0;
    TaskMain->prio_est = 0;

    TaskMain->next = NULL;
    TaskMain->prev = NULL;

    queue_append((queue_t**)&Tasks, (queue_t*)TaskMain);

    //Atribui à tarefa atual à main:
    TaskAtual = TaskMain;

    // Inicialização do timer de 1ms

  // registra a ação para o sinal de timer SIGALRM
  action.sa_handler = tratador ;
  sigemptyset (&action.sa_mask) ;
  action.sa_flags = 0 ;

  if (sigaction (SIGALRM, &action, 0) < 0)
  {
    perror ("Erro em sigaction: ") ;
    exit (1) ;
  }

  // ajusta valores do temporizador
  // Ajustado para 1 ms

  timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
  timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
  timer.it_interval.tv_usec =  1000 ;   // disparos subsequentes, em micro-segundos
  timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos


      // arma o temporizador ITIMER_REAL (vide man setitimer)
  if (setitimer (ITIMER_REAL, &timer, 0) < 0)
  {
    perror ("Erro em setitimer: ") ;
    exit (1) ;
  }

  // Faz o Dispatcher atuar no escalonamento de tarefas
  task_yield();

}


// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,void (*start_func)(void *),void *arg){			// argumentos para a tarefa

	// Criando o contexto para a tarefa desejada.
	getcontext(&(task->context));

	char *stack;
	stack = malloc (STACKSIZE) ;
	   if (stack)
	   {
	      task->context.uc_stack.ss_sp = stack ;
	      task->context.uc_stack.ss_size = STACKSIZE;
	      task->context.uc_stack.ss_flags = 0;
	      task->context.uc_link = 0;
	   }
	   else
	   {
	      perror ("Erro na criação da pilha: ");
	      exit (1);
	   }

	//Atribuindo à tarefa, suas respectivas funções e argumentos
	makecontext (&(task->context),(void *)(*start_func),1,arg);

	task->tid = ID_TASKS++;
    task->Ticks = 20;

    //Seta o tempo de execução que a tarefa iniciou
    task->time_init = systime();
    task->time_exit = 0;
    task->processor_time = 0;
    task->activations = 0;
    // printf("Tarefa %d criada\n", task->tid);
    //printf("Task: %p \n",task);

    //Ao ser criada, cada tarefa recebe a prioridade default (0).
    task->old = 0;
    task->prio_est = 0;
    task->estado = 0; // Todas as tarefas iniciam ativas


    queue_append((queue_t**)&Tasks, (queue_t*) task);

	//Essa função retorna o ID, mas ainda não atribuímos valores aos ID's
	return(task->tid);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode){
    //printf("EXIT\n");
    task_t* aux = TaskAtual;

    //Seta o tempo de execução que a tarefa iniciou
    TaskAtual->time_exit = systime();

    depend_aux = (TaskAtual->Suspensas);
    //Fila de dependentes auxiliar, apenas para percorrer a fila
    while(TaskAtual->Suspensas){
        //printf("Chamei pra acordar\n");
        task_resume(depend_aux);
        depend_aux = depend_aux->next;
    }


    queue_remove((queue_t**)&Tasks, (queue_t*) aux);
    printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", TaskAtual->tid, (TaskAtual->time_exit - TaskAtual->time_init ), TaskAtual->processor_time, TaskAtual->activations);

    TaskAtual->estado = 3;

    if (TaskAtual->tid != 0){
	    //Sempre retorna ao escalonador
        //printf("Chama o Dispatcher\n");
    	task_yield();
    }
};

// alterna a execução para a tarefa indicada
int task_switch (task_t *task){
    //printf("Estou no switch\n");
    //Incremento do Task Activations
    task->activations++;

    task_t* anterior;
    anterior = TaskAtual;
    TaskAtual = task;

    int result = swapcontext(&(anterior->context), &(task->context));
    //printf("%d", result);

    if (result == -1){
      return -1;
    }
    else{
        return 0;
    }
};

// retorna o identificador da tarefa corrente (main eh 0)
int task_id (){
    return TaskAtual->tid;
};

void task_suspend (task_t *task, task_t **queue){
//Suspende uma tarefa, retirando-a de sua fila atual,
//adicionando-a à fila queue e mudando seu estado para “suspensa”.
//Se task for nulo, considera a tarefa corrente.
//Se queue for nulo, não retira a tarefa de sua fila atual.
//queue_t* aux;

    if(task == NULL){ //Task NULL, considerar a TaskAtual

        //printf("Suspendendo a Tarefa Atual\n");

        if(queue == NULL){
            //printf("Queue NULL\n");
            task_yield();
            //Não retira tarefa da sua fila atual
        }
        else{
            //printf("Colocando tarefa na fila queue\n");
            task_t* aux = (task_t*) queue_remove((queue_t**)&Tasks, (queue_t*) TaskAtual);
            //printf("Removid tarefa\n");
            queue_append((queue_t**)queue, (queue_t*)aux);
            //printf("Coloquei na fila\n");
        }
    }
    else{
        if(queue == NULL){
            //Não retira tarefa da sua fila atual
            task_yield();
        }
        else{
            task = (task_t*) queue_remove((queue_t**)&Tasks, (queue_t*) task);
            queue_append((queue_t**)queue, (queue_t*) task);
        }
    }
};

void task_resume (task_t *task){
//Acorda uma tarefa, retirando-a de sua fila atual (se estiver em uma fila),
//adicionando-a à fila de tarefas prontas (ready queue) e mudando seu estado para “pronta”.
    //printf("Acordando a tarefa %d\n", task->tid);
    //printf("Entrei no resume\n");

    if(task->estado == 1){ //Significa que a tarefa está na fila de Suspensas
        //printf("Removendo da fila de Adormecidas\n");
        task_t* aux = (task_t*)queue_remove((queue_t**)&TaskAtual->Suspensas, (queue_t*)task);
        queue_append((queue_t**)&Tasks, (queue_t*)aux);
        //Inicia a tarefa com sua prioridade estática
        aux->old = aux->prio_est;
    }
    //Acorda as adormecidas e as que estão na barreira
    else if(task->estado == 2){
        task_t* aux = (task_t*)queue_remove((queue_t**)&sleep_queue, (queue_t*)task);
        queue_append((queue_t**)&Tasks, (queue_t*)aux);
        //Inicia a tarefa com sua prioridade estática
        aux->old = aux->prio_est;
    }

    task->estado = 0; //Muda o estado da tarefa para pronta

};

int task_join (task_t *task){
//Caso a tarefa b não exista ou já tenha encerrado,
//esta chamada deve retornar imediatamente, sem suspender a tarefa corrente.

    //printf("Entrei no Join\n");
    if(task == NULL){
        //printf("Task == NULL\n");
        return -1;
    }
    if(task->estado == 3){
        //printf("Tarefa exit\n");
        return -1;
    }
    else{
        //printf("Suspendendo tarefa atual\n");
        task_suspend(NULL, &(task->Suspensas));
        TaskAtual->estado = 1; //Muda o estado da tarefa

        //printf("Voltei para o Join\n");
        task_switch(task);
        return (Exit_Code_Join);
    }

};


// operações de gestão do tempo ================================================

// suspende a tarefa corrente por t segundos
void task_sleep (int t){

    //Suspendo a tarefa e coloco na fila de prontas
    task_suspend(NULL, &sleep_queue);
    TaskAtual->estado = 2;

    //Se adormece por t = 0, acorde a tarefa
    if(t == 0){
        task_resume(TaskAtual);
    }
    else{
        //calcula o tempo que ela deve ficar adormecida
        TaskAtual->acorda = systime() + (t*1000);
        //printf("Acordar em: %dms \n", TaskAtual->acorda);
    }

    task_yield();

};

// retorna o relógio atual (em milisegundos)
unsigned int systime (){
    return Relogio;
};

//Funções do Semáforo =============================================================================

// cria um semáforo com valor inicial "value"
int sem_create (semaphore_t *s, int value){
    //printf("Criando Semáforo\n");
    // Desativa flag de preempcao
    Preempcao_ativa = 0;
    if(s){
        //Inicia o contador com o valor
        s->contador = value;
        //Fila vazia
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return 0;
    }
    else{
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// requisita o semáforo
int sem_down (semaphore_t *s){
    // printf("Down\n");

    // Muda o flag da preempcao
    Preempcao_ativa = 0;

    //Garante que o semáforo existe
    if(s){
        //Contado do semáforo decrementa
        s->contador = s->contador - 1;
        // printf("Contador semáforo: %d", s->contador);
        if(s->contador < 0){
            // printf("Down Contador < 0\n");
            //Suspende a tarefa atual e coloca na fila do semáforo
            // Volta pra 1 o flag de preempcao
            Preempcao_ativa = 1;
            task_suspend(NULL, &(s->Fila));
            // printf("Tarefa %d suspensa\n", TaskAtual->tid);
            //Execução volta pro Dispatcher
            task_yield();
        }
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        //Caso esteja tudo certo, ele retorna 0
        return 0;

    }
    else{
    //Caso o semáforo não exista ou já tenha sido desativado
    // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// libera o semáforo
int sem_up (semaphore_t *s){
    // printf("UP\n");
    // Desativa preempcao
    Preempcao_ativa = 0;

    if(s){
        //Contado do semáforo incrementa
        s->contador = s->contador + 1;

        if(queue_size((queue_t*)s->Fila) > 0){
            //printf("UP Fila maior que zero\n");
            //Remove o primeiro elemento da Fila
            task_t* aux = (task_t*) queue_remove((queue_t**)&(s->Fila), (queue_t*)s->Fila);

            //retorna a tarefa para a fila de prontas
            aux->estado = 0;
            queue_append((queue_t**)&Tasks, (queue_t*)aux);
        }
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return 0;
    }
    else{ //Caso o semáforo não exista ou já tenha sido desativado
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// destroi o semáforo, liberando as tarefas bloqueadas
int sem_destroy (semaphore_t *s){
        //printf("Destroy\n");
    // Desativa preempcao
    Preempcao_ativa = 0;
    if(s){
        while(queue_size((queue_t*)s->Fila) > 0){
            //Remove o primeiro elemento da Fila
            task_t* aux = (task_t*) queue_remove((queue_t**)&(s->Fila), (queue_t*)s->Fila);

            //retorna a tarefa para a fila de prontas
            aux->estado = 0;
            queue_append((queue_t**)&Tasks, (queue_t*)aux);
        }

        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return 0;
    }
    else{
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// Barreira =========================================================================================
// Inicializa uma barreira
int barrier_create (barrier_t *b, int N){

    //Verifica se a barreira existe e se N é diferente de zero
    if(b && N > 0){
        //Inicia a quantidade de valores que devem deixer a barreira com N
        b->quant_leave = N;

        //Atribui o valor ao semáforo
        sem_create((b->semaph), 1);

        return 0;
    }
    else
        return -1;
};

// Chega a uma barreira
int barrier_join (barrier_t *b){
    //Verifica se a barreira existe
    if(b){

        //Down no semáforo
        sem_down((b->semaph));

        //Inicia a quantidade de valores que devem deixer a barreira com N
        //e as tarefas que chegaram com zero
        if((queue_size((queue_t*)b->Barreira)) < b->quant_leave){

            //Identifica que existe Barreira Criada
            Barreira = 1;

            //Suspende a Tarefa Atual para a Fila da Barreira
            task_suspend(NULL, &(b->Barreira));

            //printf("Quantidade de Tarefas na Barreira: %d Liberar: %d\n", (queue_size((queue_t*)b->Barreira)), b->quant_leave);

            //Indica que a tarefa está na Barreira
            TaskAtual->estado = 4;

        }

        if((queue_size((queue_t*)b->Barreira)) == b->quant_leave){

            //printf("Liberando Barreira");

            //Identifica que não haverá mais Tarefas esperando na Barreira
            Barreira = 0;

            task_t* Fila = b->Barreira->prev;
            //Percorre toda a fila da Barreira e remove todas
            while((queue_size((queue_t*)b->Barreira)) > 0){

                task_t* prev = Fila->prev;

                //printf("Tamanho da Fila: %d\n", (queue_size((queue_t*)b->Barreira)));

                //Remove da Fila da Barreira e coloca na fila de Prontas
                task_t* aux = (task_t*)queue_remove((queue_t**)&(b->Barreira), (queue_t*)Fila);
                queue_append((queue_t**)&Tasks, (queue_t*)aux);

                //Anda para a próxima tarefa
                Fila = prev;

            }

        }

        //Libera Semáforo
        sem_up((b->semaph));


        task_yield();

        return 0;
    }
    else
        return -1;

};

// Destrói uma barreira
int barrier_destroy (barrier_t *b){
    //Verifica se a barreira existe
    if(b){

        //Identifica que não haverá mais Tarefas esperando na Barreira
        Barreira = 0;

        task_t* Fila = b->Barreira;
            //Percorre toda a fila da Barreira e remove todas
            if(queue_size((queue_t*)b->Barreira) > 0){
                while(Fila->next != b->Barreira){

                    task_t* next = Fila->next;
                    task_t* aux = (task_t*)queue_remove((queue_t**)&(b->Barreira), (queue_t*)Fila);
                    queue_append((queue_t**)&Tasks, (queue_t*)aux);

                    //Anda para a próxima tarefa
                    Fila = next;

                }
            }

        //destroi o semáforo
        sem_destroy((b->semaph));

        return 0;
    }
    else
        return -1;

};


// Filas de Mensagens ======================================================================================

// cria uma fila para até max mensagens de size bytes cada
int mqueue_create (mqueue_t *queue, int max, int size){
    // Desativa preempcao
    Preempcao_ativa = 0;
    //Aloca um vetor de inteiros com o tamanho recebido
    if(queue && max > 0 && size > 0){

        //Guarda os dados recebidos
        queue->max = max;
        queue->size = size;
        //Inicializa a variável vaga = 0
        queue->vaga = 0;
        queue->consumido = 0;
        queue->quantidade = 0;
        queue->active =1;

        //Aloca um vetor do tamanho necessário
        queue->mensagem = malloc(max*size);

        //Cria semáforos
        sem_create(&queue->sem_buffer, 1);
        sem_create(&queue->sem_msg, 0);
        sem_create(&queue->sem_vagas, max);

        // printf("Criei fila de mensagens; Vaga ocupada = %d; Consumido = %d; Quantidade = %d\n", queue->vaga, queue->consumido, queue->quantidade);

        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return 0;
    }
    else{
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// envia uma mensagem para a fila
int mqueue_send (mqueue_t *queue, void *msg){
    //Desativa preempcao
    Preempcao_ativa = 0;

    if(queue != NULL && msg != NULL && queue->active){

        Preempcao_ativa = 1;
        task_sleep(1);

        //Bloqueia os semáforos
        sem_down(&queue->sem_vagas);
        sem_down(&queue->sem_buffer);

        //Desativa preempcao
        Preempcao_ativa = 0;

        //Copia uma msg de tamanho size para o vetor na posição vaga
        bcopy(msg, queue->mensagem + queue->vaga*queue->size, queue->size);

        // incrementa a vaga
        queue->vaga = queue->vaga + 1;

        //Incrementa a quantidade de mensagens
        queue->quantidade = queue->quantidade + 1;

        //Zera o semáforo
        if(queue->vaga >= 5){
                queue->vaga = 0;
        }

        // printf("Enviei mensagem - Quantidade de msg: %d \n", queue->quantidade);

        //Libera semáforos do buffer e do item
        sem_up(&queue->sem_buffer);
        sem_up(&queue->sem_msg);

        // Ja esta ativo o flag de preempcao
        return 0;
    }
    else{
        // Volta pra 1 o flag de preempcao
        Preempcao_ativa = 1;
        return -1;
    }
};

// recebe uma mensagem da fila
int mqueue_recv (mqueue_t *queue, void *msg){

    Preempcao_ativa = 0;

    if(queue != NULL && msg != NULL && queue->active){
        //Verifica se os semáforos estão disponíveis
        sem_down(&queue->sem_msg);
        sem_down(&queue->sem_buffer);

        Preempcao_ativa = 0;
        //Retira msg do buffer
        //Copia uma msg de tamanho size para o ponteiro msg
        bcopy((queue->mensagem + queue->consumido*queue->size), msg, queue->size);

        //Incrementa a posição consumida
        queue->consumido = queue->consumido + 1;

        //Decrementa a quantidade de mensagens
        queue->quantidade = queue->quantidade - 1;

        //Zera a posição consumida
        if(queue->consumido >= 5){
            queue->consumido = 0;
        }

        // printf("Recebi mensagem - Quantidade de msg: %d \n", mqueue_msgs(queue));

        //Libera os Semáforos
        sem_up(&queue->sem_buffer);
        sem_up(&queue->sem_vagas);

        // Desativada a preempcao
        Preempcao_ativa = 1;
        task_sleep(1);

        return 0;
    }
    else{
        Preempcao_ativa = 1;
        return -1;
    }
}

// destroi a fila, liberando as tarefas bloqueadas
int mqueue_destroy (mqueue_t *queue){

    Preempcao_ativa = 0;

    if(queue != NULL && queue->active){
        //Destrói semáforos
        sem_destroy(&queue->sem_buffer);
        sem_destroy(&queue->sem_msg);
        sem_destroy(&queue->sem_vagas);
        queue->active = 0;

        //printf("Fim semaforo \n");

        //Libera vetor
        free(queue->mensagem);

        //printf("Fim Fila de mensagens \n");

        return 0;
    }
    else{
        Preempcao_ativa = 0;
        return -1;
    }
}

// informa o número de mensagens atualmente na fila
int mqueue_msgs (mqueue_t *queue){
    Preempcao_ativa = 0;
    if(queue != NULL && queue->active){
        //printf("Quantidade de msg na fila: %d \n", queue->quantidade);
        Preempcao_ativa = 1;
        return queue->quantidade;
    }
    else{
        Preempcao_ativa = 1;
        return -1;
    }
};

