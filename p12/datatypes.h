// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional

#ifndef __DATATYPES__
#define __DATATYPES__
#include <ucontext.h>


// Estrutura que define uma tarefa
typedef struct task_t
{
	struct task_t *prev, *next ; // Para usar filas 
	int tid; 					 // Task_id
	ucontext_t context;			 // Utilizado para contexto
	void *stack;				 // Pilha interna da memória
	struct task_t *parent; 		 // Tarefa pai
    int old;					 // Prioridade estática
    int prio_est;				 // Prioridade Dinâmica
    int Ticks;					 // Quantuns
	// enum status_t status;		
	// preencher quando necessário
    int time_init;				 // Timestamp de início da tarefa
    int time_exit;				 // Timestamp de fim da tarefa
    int processor_time;			 // Timestamp do processador
    int activations;			 // Número de ativações da tarefa

  //Fila de Tarefas que dependem do encerramento para serem acordadas
  struct task_t *Suspensas;
  int estado; // 0 = Ativa ; 1 = Suspensa ; 2 = Adormecida ; 3 = Exit ; 4 = Barreira;

  //Trata a tarefa adormecida
  int acorda;
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  int contador;
  struct task_t* Fila;

} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  int quant_leave;
  struct task_t* Barreira;
  semaphore_t* semaph;

} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  void *mensagem;
  int max;
  int size;
  int vaga;
  int consumido;
  int quantidade;
  int active;
  semaphore_t sem_vagas;
  semaphore_t sem_msg;
  semaphore_t sem_buffer;
} mqueue_t ;

#endif
