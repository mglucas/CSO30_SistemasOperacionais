#include "pingpong.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */
int ID_TASKS = 0;

task_t* TaskAtual = NULL;
task_t* depend_aux = NULL;
task_t* TaskMain = NULL;
task_t* TaskDispatcher = NULL;

task_t* Tasks; // Fila da prontas 
task_t* sleep_queue; // Fila da adormecidas 

int Exit_Code_Join = 3;  //Código de Erro de uma tarefa Join

int Relogio = 0;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

// operações de escalonamento ==================================================

// Task_yield - Faz o switch para o dispatcher
void task_yield (){
    task_switch(TaskDispatcher);    
};


// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
    if(task){
        task->prio_est = prio;
        task->old = task->prio_est;
    }
    else{
        TaskAtual->prio_est = prio;
        TaskAtual->old = TaskAtual->prio_est;
    }
    //printf("Prioridade da Tarefa %d: %d\n", task->tid, task->prio);

};

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
    if(task){
        return task->prio_est;
    }
    else
        return TaskAtual->prio_est;
};

// operações do Dispatcher ==================================================

//Função que determina qual a próxima tarefa à ser executada - Escalonador
//Implementa o escalonador por prioridade 
task_t* scheduler(){
    //Verifica qual é a menor prioridade entre as tarefas
    task_t* next_task = NULL;
    task_t* aux = TaskDispatcher->next;

    //O nosso sistema de prioridade está na escala de -20 à 20
    int prio_min = 21;        //Recebe uma prioridade alta

    //printf("Estou no scheduler\n");
    if(TaskDispatcher->next == TaskDispatcher){
    //Se o next do Dispatcher for o Dispatcher, retorna NULL
        // printf("Próxima tarefa é a Dispatcher\n");
        return NULL;
    }
    else{
    //Verifica toda a lista de Tarefas - excluindo Dispatcher
        while(aux != TaskDispatcher){
            if(aux->old < prio_min){
                prio_min = aux->old;

                //Se outro next foi selecionado antes, mas não é o menor, 
                //então decremente também seu old
                if(next_task){
                    next_task->old-=1;
                }

                next_task = aux;
            }
            else{
                aux->old-=1;
            }
            
            aux = aux->next;
        }

        //Retorna o que tem menor prioridade - que será executado primeiro
        //printf("next: %p\n", next_task);
        next_task->old = next_task->prio_est;
        return next_task;
    }
}

//Função atribuida ao Dispatcher
void dispatcher_body(){    
    //Ponteiro que armazena a próxima tarefa a ser executada, enviada pelo scheduler
    task_t* next;

    //Enquanto a Fila de tarefas Prontas não for vazia
    //E a Fila de Adormecidas não for vazia
    while( (queue_size((queue_t*)Tasks) - 1) > 0 || (queue_size((queue_t*)sleep_queue) > 0)){

        task_t* aux = sleep_queue;
        if(queue_size((queue_t*)sleep_queue) > 0){
            //printf("Tamanho Sleep queue: %d\n",queue_size((queue_t*)sleep_queue));
            int tamanho = queue_size((queue_t*)sleep_queue);
            for(int i=0; i<tamanho; i++){
                //printf("Acordar: %d; id: %d; Relógio: %d\n", aux->acorda, aux->tid, systime());
                if(aux->acorda < systime()){
                    task_resume(aux);
                    aux = aux->next; 
                    break;                  

                }
                else{
                    aux = aux->next;
                }
            }
        }
        
        //Atribui próxima tarefa ao next
        next = scheduler();
        //printf("Next-Task: %p\n", next);

        if(next){
            //Faz o switch para a próxima tarefa
            task_switch(next);
        }
    }

    task_exit(0);
}

void tratador (int signum)
{
  //printf ("Recebi o sinal %d\n", signum) ;
  TaskAtual->Ticks--;

  Relogio++;
  TaskAtual->processor_time++;

  if(TaskAtual->Ticks==1){
        TaskAtual->Ticks=20;
        task_yield();
        
  }

}

// Funções de Inicialização do Sistema  ==================================================
// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init (){
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0);

    //Cria o Dispatcher
    TaskDispatcher = malloc(sizeof(task_t));
    task_create(TaskDispatcher, dispatcher_body, 0);

    //Inicializa a main com o contexto atual executado
    TaskMain= malloc(sizeof(task_t));
    getcontext(&(TaskMain->context));
    TaskMain->tid = ID_TASKS++;    
    
    TaskMain->Ticks = 20;

    //Seta o tempo de execução que a tarefa iniciou
    TaskMain->time_init = systime();
    TaskMain->time_exit = 0;
    TaskMain->processor_time = 0;
    TaskMain->activations = 0;
    //printf("Tarefa %d criada\n", task->tid);
    //printf("Task: %p \n",task);    
    
    //Ao ser criada, cada tarefa recebe a prioridade default (0).
    TaskMain->old = 0;
    TaskMain->prio_est = 0;

    TaskMain->next = NULL;
    TaskMain->prev = NULL;
    
    queue_append((queue_t**)&Tasks, (queue_t*)TaskMain);
    
    //Atribui à tarefa atual à main:
    TaskAtual = TaskMain;

    // Inicialização do timer de 1ms 

  // registra a ação para o sinal de timer SIGALRM
  action.sa_handler = tratador ;
  sigemptyset (&action.sa_mask) ;
  action.sa_flags = 0 ;

  if (sigaction (SIGALRM, &action, 0) < 0)
  {
    perror ("Erro em sigaction: ") ;
    exit (1) ;
  }

  // ajusta valores do temporizador
  // Ajustado para 1 ms

  timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
  timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
  timer.it_interval.tv_usec =  1000 ;   // disparos subsequentes, em micro-segundos
  timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos


      // arma o temporizador ITIMER_REAL (vide man setitimer)
  if (setitimer (ITIMER_REAL, &timer, 0) < 0)
  {
    perror ("Erro em setitimer: ") ;
    exit (1) ;
  }

  // Faz o Dispatcher atuar no escalonamento de tarefas
  task_yield();
    
}


// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg){			// argumentos para a tarefa

	// Criando o contexto para a tarefa desejada.
	getcontext(&(task->context));

	char *stack;
	stack = malloc (STACKSIZE) ;
	   if (stack)
	   {
	      task->context.uc_stack.ss_sp = stack ;
	      task->context.uc_stack.ss_size = STACKSIZE;
	      task->context.uc_stack.ss_flags = 0;
	      task->context.uc_link = 0;
	   }
	   else
	   {
	      perror ("Erro na criação da pilha: ");
	      exit (1);
	   }

	//Atribuindo à tarefa, suas respectivas funções e argumentos
	makecontext (&(task->context),(void *)(*start_func),1,arg);

	task->tid = ID_TASKS++;
    task->Ticks = 20;

    //Seta o tempo de execução que a tarefa iniciou
    task->time_init = systime();
    task->time_exit = 0;
    task->processor_time = 0;
    task->activations = 0;
    //printf("Tarefa %d criada\n", task->tid);
    //printf("Task: %p \n",task);    
    
    //Ao ser criada, cada tarefa recebe a prioridade default (0).
    task->old = 0;
    task->prio_est = 0;
    task->estado = 0; // Todas as tarefas iniciam ativas


    queue_append((queue_t**)&Tasks, (queue_t*) task);
        
	//Essa função retorna o ID, mas ainda não atribuímos valores aos ID's
	return(task->tid);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode){
    //printf("EXIT\n");
    task_t* aux = TaskAtual;

    //Seta o tempo de execução que a tarefa iniciou
    TaskAtual->time_exit = systime();

    depend_aux = (TaskAtual->Suspensas);
    //Fila de dependentes auxiliar, apenas para percorrer a fila
    while(TaskAtual->Suspensas){
        //printf("Chamei pra acordar\n");
        task_resume(depend_aux);
        depend_aux = depend_aux->next;
    }


    queue_remove((queue_t**)&Tasks, (queue_t*) aux);
    printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", TaskAtual->tid, (TaskAtual->time_exit - TaskAtual->time_init ), TaskAtual->processor_time, TaskAtual->activations);

    TaskAtual->estado = 3;
    
    if (TaskAtual->tid != 0){
	    //Sempre retorna ao escalonador
        //printf("Chama o Dispatcher\n");
    	task_yield();
    }

};

// alterna a execução para a tarefa indicada
int task_switch (task_t *task){
    //printf("Estou no switch\n");
    //Incremento do Task Activations    
    task->activations++;

    task_t* anterior;
    anterior = TaskAtual;
    TaskAtual = task;

    int result = swapcontext(&(anterior->context), &(task->context));
    //printf("%d", result);

    if (result == -1){
      return -1;
    }
    else{
        return 0;
    }
    
};

// retorna o identificador da tarefa corrente (main eh 0)
int task_id (){
    return TaskAtual->tid;
};

void task_suspend (task_t *task, task_t **queue){
//Suspende uma tarefa, retirando-a de sua fila atual, 
//adicionando-a à fila queue e mudando seu estado para “suspensa”. 
//Se task for nulo, considera a tarefa corrente. 
//Se queue for nulo, não retira a tarefa de sua fila atual.
//queue_t* aux;

    if(task == NULL){ //Task NULL, considerar a TaskAtual

        //printf("Suspendendo a Tarefa Atual\n");
        
        if(queue == NULL){
            task_yield();
            //Não retira tarefa da sua fila atual
        }
        else{
            task_t* aux = (task_t*) queue_remove((queue_t**)&Tasks, (queue_t*) TaskAtual);
            queue_append((queue_t**)queue, (queue_t*)aux);
        }
    }
    else{
        if(queue == NULL){
            //Não retira tarefa da sua fila atual
            task_yield();
        }
        else{
            task = (task_t*) queue_remove((queue_t**)&Tasks, (queue_t*) task);
            queue_append((queue_t**)queue, (queue_t*) task);
        }   
    }

};

void task_resume (task_t *task){
//Acorda uma tarefa, retirando-a de sua fila atual (se estiver em uma fila), 
//adicionando-a à fila de tarefas prontas (ready queue) e mudando seu estado para “pronta”.
    //printf("Acordando a tarefa %d\n", task->tid);
    //printf("Entrei no resume\n");
    
    if(task->estado == 1){ //Significa que a tarefa está na fila de Suspensas
        //printf("Removendo da fila de Adormecidas\n");
        task_t* aux = (task_t*)queue_remove((queue_t**)&TaskAtual->Suspensas, (queue_t*)task);
        queue_append((queue_t**)&Tasks, (queue_t*)aux);
        //Inicia a tarefa com sua prioridade estática
        aux->old = aux->prio_est;
    }
    else if(task->estado == 2){
        task_t* aux = (task_t*)queue_remove((queue_t**)&sleep_queue, (queue_t*)task);
        queue_append((queue_t**)&Tasks, (queue_t*)aux);
        //Inicia a tarefa com sua prioridade estática
        aux->old = aux->prio_est;
    }

    task->estado = 0; //Muda o estado da tarefa para pronta

};

int task_join (task_t *task){    
//Caso a tarefa b não exista ou já tenha encerrado, 
//esta chamada deve retornar imediatamente, sem suspender a tarefa corrente. 

    //printf("Entrei no Join\n");
    if(task == NULL){
        //printf("Task == NULL\n");
        return -1;
    }
    if(task->estado == 3){
        //printf("Tarefa exit\n");
        return -1;
    }
    else{
        //printf("Suspendendo tarefa atual\n");
        task_suspend(NULL, &(task->Suspensas));
        TaskAtual->estado = 1; //Muda o estado da tarefa

        //printf("Voltei para o Join\n");
        task_switch(task);
        return (Exit_Code_Join);
    }

};


// operações de gestão do tempo ================================================

// suspende a tarefa corrente por t segundos
void task_sleep (int t){

    //Suspendo a tarefa e coloco na fila de prontas
    task_suspend(NULL, &sleep_queue);
    TaskAtual->estado = 2;

    //Se adormece por t = 0, acorde a tarefa
    if(t == 0){
        task_resume(TaskAtual);
    }
    else{
        //calcula o tempo que ela deve ficar adormecida
        TaskAtual->acorda = systime() + (t*1000);
        //printf("Acordar em: %dms \n", TaskAtual->acorda);
    }

    task_yield();

};

// retorna o relógio atual (em milisegundos)
unsigned int systime (){
    return Relogio;
};


