#include "pingpong.h"
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */
int ID_TASKS = 0;

task_t* TaskAtual = NULL;
task_t* TaskMain = NULL;
task_t* TaskDispatcher = NULL;

task_t* Tasks;

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init (){
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0);

    //Inicializa a main com o contexto atual executado
    //printf("Criando a Tarefa Main: \n");
    TaskMain= malloc(sizeof(task_t));
    getcontext(&(TaskMain->context));
    TaskMain->tid = ID_TASKS++;    
    
    TaskMain->next = NULL;
    TaskMain->prev = NULL;
    
    queue_append((queue_t**)&Tasks, (queue_t*)TaskMain);
    //printf("TaskMain: %p \n",TaskMain);    
    //printf("TaskMain->next: %p \n",TaskMain->next);
    //printf("TaskMain->prev: %p \n",TaskMain->prev);
    
    //Atribui à tarefa atual à main:
    //printf("Atribuindo à Tarefa Atual a Main: \n");
    TaskAtual = TaskMain;

    //task_create(&TaskDispatcher, )
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg){			// argumentos para a tarefa

	// Criando o contexto para a tarefa desejada.
	getcontext(&(task->context));

	char *stack;
	stack = malloc (STACKSIZE) ;
	   if (stack)
	   {
	      task->context.uc_stack.ss_sp = stack ;
	      task->context.uc_stack.ss_size = STACKSIZE;
	      task->context.uc_stack.ss_flags = 0;
	      task->context.uc_link = 0;
	   }
	   else
	   {
	      perror ("Erro na criação da pilha: ");
	      exit (1);
	   }

	//Atribuindo à tarefa, suas respectivas funções e argumentos
	makecontext (&(task->context),(void *)(*start_func),1 ,arg);

	task->tid = ID_TASKS;

    queue_append((queue_t**)&Tasks, (queue_t*) task);

    ID_TASKS++;

	//Essa função retorna o ID, mas ainda não atribuímos valores aos ID's
	return(task->tid);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exitCode){

    task_t* aux;
    aux = TaskAtual;

    queue_remove((queue_t**)&Tasks, (queue_t*) aux);

    if(TaskAtual->tid == 1){ 
           task_switch(TaskMain);    
    }
    else{
        //task_switch(TaskDispatcher);
        task_switch(TaskMain);
    }

};

// alterna a execução para a tarefa indicada
int task_switch (task_t *task){
    
    task_t* anterior;
    anterior = TaskAtual;
    TaskAtual = task;

        //printf("anterior->prev: %p \n",anterior->prev);
        //printf("anterior->next: %p \n",anterior->next);
//
        //printf("TaskAtual->prev: %p \n",TaskAtual->prev);
        //printf("TaskAtual->next: %p \n",TaskAtual->next);  


    int result = swapcontext(&(anterior->context), &(task->context));
    //printf("%d", result);

    if (result == -1){
      return -1;
    }
    else{
        return 0;
    }
    
};

// retorna o identificador da tarefa corrente (main eh 0)
int task_id (){
    return TaskAtual->tid;
};
